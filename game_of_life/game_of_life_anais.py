import numpy as np
from pytest import *
import matplotlib.pyplot as plt
import matplotlib.animation as animation
#from scipy import misc
from tkinter import*
import tkinter as tk
from tkinter import Canvas
import time
from tkinter import Tk, StringVar, Label, Entry, Button
from functools import partial


#création d'un uniuvers de taille size
def generate_universe(size):
    universe = np.zeros((size[0],size[1]))
    return universe
    
#ceci est un commentaire inutile

def test_generate_universe():
    assert generate_universe((4,4)).any() == np.zeros((4,4)).any()

#création d'une graine à partir du dictionnaire de graines
def create_seed(type_seed):
    return np.array(seeds[type_seed])


#dictionnaire de graines
seeds = {
    "boat": [[1, 1, 0], [1, 0, 1], [0, 1, 0]],
    "r_pentomino": [[0, 1, 1], [1, 1, 0], [0, 1, 0]],
    "beacon": [[1, 1, 0, 0], [1, 1, 0, 0], [0, 0, 1, 1], [0, 0, 1, 1]],
    "acorn": [[0, 1, 0, 0, 0, 0, 0], [0, 0, 0, 1, 0, 0, 0], [1, 1, 0, 0, 1, 1, 1]],
    "block_switch_engine": [
        [0, 0, 0, 0, 0, 0, 1, 0],
        [0, 0, 0, 0, 1, 0, 1, 1],
        [0, 0, 0, 0, 1, 0, 1, 0],
        [0, 0, 0, 0, 1, 0, 0, 0],
        [0, 0, 1, 0, 0, 0, 0, 0],
        [1, 0, 1, 0, 0, 0, 0, 0],
    ],
    "infinite": [
        [1, 1, 1, 0, 1],
        [1, 0, 0, 0, 0],
        [0, 0, 0, 1, 1],
        [0, 1, 1, 0, 1],
        [1, 0, 1, 0, 1],
    ],
}

def test_create_seed():
    seed=create_seed(type_seed = "r_pentomino")
    assert seed.any() == np.array([[0,1,1],[1,1,0],[0,1,0]]).any()

#ajoute la graine dans l'universà la place x_start, y_start
def add_seed_to_universe(seed,universe, x_start, y_start):
    (n,p)=np.shape(seed)
    for i in range(n):
        for j in range(p):
            universe[x_start+i,y_start+j]=seed[0+i,0+j]
    return universe

def test_add_seed_to_universe():
    seed = create_seed(type_seed = "r_pentomino")
    universe = generate_universe(size=(6,6))
    universe = add_seed_to_universe(seed, universe,x_start=1, y_start=1)
    test_equality=np.array(universe ==np.array([[0,0, 0, 0, 0, 0],
 [0, 0, 1, 1, 0, 0],
 [0, 1, 1, 0, 0, 0],
 [0 ,0, 1, 0, 0, 0],
 [0 ,0, 0, 0, 0, 0],
 [0 ,0, 0, 0, 0, 0]],dtype=np.uint8))
    assert test_equality.all()

#affiche l'univers avec la graine
def display_the_universe():
    seed = create_seed(type_seed = "r_pentomino")
    universe = generate_universe(size=(6,6))
    universe = add_seed_to_universe(seed,universe,x_start=1,y_start=1)
    plt.imshow(universe,cmap='Greys')
    plt.show()





#création de l'univers à t=0, avec la graine. Ce nouveau programme limite le nombre d'erreurs possibles (graine trop ou mal placée)
def init_universe(amorce, nombre_lignes_univers, nombre_collones_univers, place_graine_x, place_graine_y):
    seed=np.array(seeds[amorce])
    nombre_ligne_universe=max(nombre_lignes_univers, np.shape(seed)[0])
    nombre_collones_univers=max(nombre_collones_univers, np.shape(seed)[1])
    universe=generate_universe((nombre_lignes_univers, nombre_collones_univers))
    if place_graine_x+np.shape(seed)[0]>nombre_lignes_univers:
        print("Choose an x_start between 0 and " + str(nombre_lignes_univers - np.shape(seed)[0]))
        place_graine_x=input()
    if place_graine_y+np.shape(seed)[1]>nombre_collones_univers:
        print("Choose an y_start between 0 and " + str(nombre_collones_univers - np.shape(seed)[1]))
        place_graine_y=input()
    universe=add_seed_to_universe(seed, universe, place_graine_x, place_graine_y)
    return universe




#créer autour de l'univers un cadre de 0 pour le comptage
def ajouter(M):
    Z = np.zeros((np.shape(M)[0]+2,np.shape(M)[1]+2))
    for i in range(np.shape(M)[0]):
        for j in range(np.shape(M)[1]):
            Z[i+1,j+1]+=M[i,j]
    return Z

def test_ajouter():
    M = np.array([[0,1,2],[3,4,5],[6,7,8]])
    test_equality=np.array(ajouter(M)==np.array([[0,0,0,0,0],[0,0,1,2,0],[0,3,4,5,0],[0,6,7,8,0],[0,0,0,0,0]],dtype=np.uint8))
    assert test_equality.all()







#création d'une matrice qui donne le nombre de voisin vivant de chaque cellule
def decompte_voisin_vivant(universe):
    U = ajouter(universe)
    F = np.zeros(np.shape(U))
    for i in range (1,np.shape(U)[0]-1):
        for j in range (1,np.shape(U)[1]-1):
            F[i,j]= U[i-1,j-1]+U[i-1,j]+U[i-1,j+1]+U[i,j-1]+U[i,j+1]+U[i+1,j-1]+U[i+1,j]+U[i+1,j+1]
    return F[1:np.shape(F)[0]-1,1:np.shape(F)[1]-1]

def test_decompte_voisin():
    M = np.array([[0,1,0],[1,0,1],[0,0,0]])
    test_equality=np.array(decompte_voisin_vivant(M)==np.array([[2,2,2],[1,3,1],[1,2,1]],dtype=np.uint8))
    assert test_equality.all()




#si la cellule est vivante ou morte changement état renvoie le nouvel univers. Il prend en paramètre l'univers précédent et la matrice contenant le nombre de voisin vivant de chaque cellule
def changement_etat(M,V):
    for i in range(np.shape(M)[0]):
        for j in range(np.shape(M)[1]):
            if M[i,j]==0 and V[i,j]==3:
                M[i,j]=1
            if M[i,j]==1 and not(V[i,j]==2 or V[i,j]==3):
                M[i,j]=0
    return M


def nouvel_univers(universe):
    M = universe
    V = decompte_voisin_vivant(universe)
    return changement_etat(M,V)

fig = plt.figure()

#generate renvoie l'univers après t itérations
def generate(t,universe):
    U=universe
    for k in range(t):
        U=nouvel_univers(universe)
    return U

def test_generate():
    U = np.array([[0,1,1,0],[0,1,0,1],[0,0,1,0],[0,0,0,0]])
    test_equality=np.array(np.array([[0,1,1,0],[0,1,0,1],[0,0,1,0],[0,0,0,0]],dtype=np.uint8)==generate(5,U))
    assert test_equality.all()

#animation de l'évolution de l'univers
t=0
def display_universe_evolution(universe,color,nb_generation,delay):
    fig = plt.figure()
    im = plt.imshow(generate(t,universe),cmap=color, animated=True)

    def updatefig(*args):
        global t
        if t>nb_generation*2:
            exit("Fin")
        t +=1
        im.set_array(generate(t,universe))
        return im,

    ani = animation.FuncAnimation(fig, updatefig, interval=delay, blit=True)
    plt.show()




#lance le jeu de la vie. L'utilisateur choisit la taille de l'univers, la graine et son emplacement, le nombre de génération et l'intervalle de temps
def game_of_life(nombre_lignes_univers,nombre_colonnes_univers,seed,place_x_de_la_graine,place_y_de_la_graine,color,nb_generation=30,delay=300):
    universe=init_universe(seed,nombre_lignes_univers,nombre_colonnes_univers,place_x_de_la_graine,place_y_de_la_graine)
    display_universe_evolution(universe,color, nb_generation,delay)

#game_of_life(10,10,"beacon",2,2,'Greens',6)


#affiche l'univers avec sa graine

def tkinter_fenetre(universe):
    gameoflife = Tk()
    f1 = Frame(gameoflife,bd=1,relief='solid')
    Label(f1,text="gameoflife").grid(row=0,column=0)
    f1.grid(row=0,column=0)
    u = Canvas(gameoflife,width=101,height=101)
    u.grid(row=1,column=0)
    for i in range(np.shape(universe)[0]):
        for j in range (np.shape(universe)[1]):
            if universe[i,j]==1:
                u.create_rectangle(i*10,j*10,(i+1)*10,(j+1)*10,fill="yellow")
            else:
                u.create_rectangle(i*10,j*10,(i+1)*10,(j+1)*10,fill="green")
    gameoflife.mainloop()

universe=init_universe("r_pentomino",10,10,1,1)


#affiche l'univers et son évolution (animation) pour n générations

def display_and_update(universe,n,couleur):
    gameoflife = tk.Tk()
    U = universe
    f1 = tk.Frame(gameoflife,bd=1,relief='solid')
    tk.Label(f1,text="gameoflife").grid(row=0,column=0)
    f1.grid(row=0,column=0)
    u = Canvas(gameoflife,width=101,height=101)
    u.grid(row=1,column=0)
    for k in range(n):
        for i in range(np.shape(universe)[0]):
            for j in range (np.shape(universe)[1]):
                if universe[i,j]==1:
                    u.create_rectangle(i*10,j*10,(i+1)*10,(j+1)*10,fill=couleur)
                else:
                    u.create_rectangle(i*10,j*10,(i+1)*10,(j+1)*10,fill="white")
        u.update()
        time.sleep(1)
        U = nouvel_univers(U)
    gameoflife.mainloop()

#affiche toutes les images de l'univers pour n générations
def display_and_update_graphical_gameoflife(universe,n):
    gameoflife = Tk()
    f1 = Frame(gameoflife,bd=1,relief='solid')
    Label(f1,text="gameoflife").grid(row=0,column=0)
    f1.grid(row=0,column=0)
    u = Canvas(gameoflife,width=10*111,height=1010)
    u.grid(row=1,column=0)
    for k in range(n):
        a=k//10
        b=k%10
        for i in range(np.shape(universe)[0]):
            for j in range (np.shape(universe)[1]):
                if universe[i,j]==1:
                    u.create_rectangle(i*10+110*b,j*10+110*a,(i+1)*10+110*b,(j+1)*10+110*a,fill="green")
                else:
                    u.create_rectangle(i*10+110*b,j*10+110*a,(i+1)*10+110*b,(j+1)*10+110*a,fill="white")
        universe=nouvel_univers(universe)
    gameoflife.mainloop()


#prend en paramètre les critères choisis par l'utilisateur dans le widget et renvoie l'animation de l'univers correspondante (couleur, temps, graine)
def tkinter_fenetre_evolution():
    universe=init_universe(seed,10,10,1,1)
    gameoflife = Tk()
    f1 = Frame(gameoflife,bd=1,relief='solid')
    Label(f1,text="gameoflife").grid(row=0,column=0)
    f1.grid(row=0,column=0)
    u = Canvas(gameoflife,width=101,height=101)
    u.grid(row=1,column=0)
    for k in range(n):
        time.sleep(timer)
        universe=nouvel_univers(universe)
        for i in range(np.shape(universe)[0]):
            for j in range (np.shape(universe)[1]):
                if universe[i,j]==1:
                    u.create_rectangle(i*10,j*10,(i+1)*10,(j+1)*10,fill=couleur)
                else:
                    u.create_rectangle(i*10,j*10,(i+1)*10,(j+1)*10,fill="white")
        u.update()
        u.delete()
    gameoflife.mainloop()

#ces fonctions renvoient la graine choisie dans une variable globale seed
def boat():
    global seed
    seed="boat"

def r_pentomino():
    global seed
    seed="r_pentomino"

def beacon():
    global seed
    seed="beacon"
#ces fonctions renvoient la couleur de la graine choisie dans une variable globale couleur
def display_and_updatered():
    global couleur
    couleur="red"

def display_and_updategreen():
    global couleur
    couleur="green"
#met à jour le texte d'un label en utilisant StringVar
def update_label(label, stringvar):
    global timer
    timer = int(stringvar.get())
    label.config(text=timer)
    stringvar.set('merci')
    print(timer)
def update_label2(label, stringvar):
    global n
    n = int(stringvar.get())
    label.config(text=n)
    stringvar.set('merci')
    print(n)
#après le choix des paramètres cette fonction renvoie l'animation de l'univers
def choix_parametre():
    root = Tk()
    frame = Frame(root)
    frame.pack()
    rouge = tk.Button(frame,
                        text="RED",
                        fg="red",
                        command=display_and_updatered)
    rouge.grid(row=0,column=0)
    vert = tk.Button(frame,
                 text="GREEN",
                 fg="green",
                 command=display_and_updategreen)
    vert.grid(row=0,column=1)
    boat_seed = Button(frame,
                   fg="blue",
                   text="boat",
                   command=boat)
    boat_seed.grid(row=1,column=0)
    r_pentomino_seed = Button(frame,
                   fg="blue",
                   text="r_pentomino",
                   command=r_pentomino)
    r_pentomino_seed.grid(row=1,column=1)
    beacon_seed = Button(frame,
                   fg="blue",
                   text="beacon",
                   command=beacon)
    beacon_seed.grid(row=1,column=2)
    text = StringVar(root)
    label = Label(root, text='Delay(ms)')
    entry_name = Entry(root, textvariable=text)
    button2 = Button(root, text='clic',
                command=partial(update_label, label, text))
    label.pack(side=tk.LEFT)
    entry_name.pack(side=tk.LEFT)
    button2.pack(side=tk.LEFT)
    text2 = StringVar(root)
    label2 = Label(root, text='itérations')
    entry_name2 = Entry(root, textvariable=text2)
    button3 = Button(root, text='clic',
                command=partial(update_label2, label2, text2))
    label2.pack(side=tk.LEFT)
    entry_name2.pack(side=tk.LEFT)
    button3.pack(side=tk.LEFT)
    start = Button(frame,
                   fg="blue",
                   text="start",
                   command=tkinter_fenetre_evolution)
    start.grid(row=3,column=0)
    button = Button(frame,
                   text="QUIT",
                   activebackground = "blue",
                   fg="red",
                   command=quit)
    button.grid(row=3,column=1)
    root.mainloop()


print(choix_parametre())






