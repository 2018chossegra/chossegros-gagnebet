from tkinter import *

def afficher_un_text(text="Hello World!"):
    window = Tk()# Window creation, root of the interface
    label_field = Label(window,text=text)# Creation of a label (text line) that says Hello World ! and with as first parameter the previous window
    label_field.pack()# Display of the label
    window.mainloop()# Running of the Tkinter loop that ends when we close the windw

#créer une ligne de saisie dans le widget
def ligne_de_saisie():
    fenetre = Tk()
    var_texte = StringVar()
    ligne_texte = Entry(fenetre, textvariable=var_texte, width=30)
    ligne_texte.pack()
    fenetre.mainloop()

#cocher des cases dans le widget

def case(textecase):
    fenetre=Tk()
    var_case = IntVar()
    case = Checkbutton(fenetre, text=textecase, variable=var_case)
    case.pack()
    fenetre.mainloop()

import tkinter as tk

def affichage(texte):
    def write_text():
        print(texte)#afficher le texte souhaité
    root = tk.Tk() #ouverture fenêtre
    frame = tk.Frame(root) #regroupement de plusieurs widget
    frame.pack()
    button = tk.Button(frame,
                   text="QUIT",
                   activebackground = "blue",
                   fg="red",
                   command=quit)#création bouton quitter
    button.pack(side=tk.LEFT)
    slogan = tk.Button(frame,
                   fg="blue",
                   text="Hello",
                   command=write_text)#création bouton teste
    slogan.pack(side=tk.LEFT)
    root.mainloop


print(affichage("ceci est un texte inutile"))
