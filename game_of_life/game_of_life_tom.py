import numpy as np
from pytest import *
import matplotlib.pyplot as plt
import matplotlib.animation as animation
#from scipy import misc
from tkinter import *
import tkinter as tk
from tkinter import Canvas
import time
from tkinter import Tk, StringVar, Label, Entry, Button
from functools import partial

import matplotlib.animation as animation
#création d'un uniuvers de taille size
def generate_universe(size):
    universe = np.zeros((size[0],size[1]))
    return universe

#ceci est un commentaire inutile

def test_generate_universe():
    assert generate_universe((4,4)).any() == np.zeros((4,4)).any()
#création d'une graine à partir du dictionnaire de graines
def create_seed(type_seed):
    return seeds[type_seed]





def test_create_seed():
    seed=create_seed(type_seed = "r_pentomino")
    assert seed.any() == np.array([[0,1,1],[1,1,0],[0,1,0]]).any()
#ajoute la graine dans l'universà la place x_start, y_start
def add_seed_to_universe(seed,universe, x_start, y_start):
    (n,p)=np.shape(seed)
    for i in range(n):
        for j in range(p):
            universe[x_start+i,y_start+j]=seed[0+i,0+j]
    return universe

def test_add_seed_to_universe():
    seed = create_seed(type_seed = "r_pentomino")
    universe = generate_universe(size=(6,6))
    universe = add_seed_to_universe(seed, universe,x_start=1, y_start=1)
    test_equality=np.array(universe ==np.array([[0,0, 0, 0, 0, 0],
 [0, 0, 1, 1, 0, 0],
 [0, 1, 1, 0, 0, 0],
 [0 ,0, 1, 0, 0, 0],
 [0 ,0, 0, 0, 0, 0],
 [0 ,0, 0, 0, 0, 0]],dtype=np.uint8))
    assert test_equality.all()


def display_the_universe():
    seed = create_seed(type_seed = "r_pentomino")
    universe = generate_universe(size=(6,6))
    universe = add_seed_to_universe(seed,universe,x_start=1,y_start=1)
    plt.imshow(universe,cmap='Greys')
    plt.show()






def init_universe(amorce, nombre_ligne_universe, nombre_collones_univers, place_graine_x, place_graine_y):
    seed=np.array(seeds[amorce])
    nombre_ligne_universe=max(nombre_ligne_universe, np.shape(seed)[0])
    nombre_collones_univers=max(nombre_collones_univers, np.shape(seed)[1])
    universe=generate_universe((nombre_ligne_universe, nombre_collones_univers))
    if place_graine_x+np.shape(seed)[0]>nombre_ligne_universe:
        print("Choose an x_start between 0 and " + str(nombre_ligne_universe - np.shape(seed)[0]))
        place_graine_x=input()
    if place_graine_y+np.shape(seed)[1]>nombre_collones_univers:
        print("Choose an y_start between 0 and " + str(nombre_collones_univers - np.shape(seed)[1]))
        place_graine_y=input()
    universe=add_seed_to_universe(seed, universe, place_graine_x, place_graine_y)
    return universe

#créer autour de l'univers un cadre de 0 pour le comptage
def ajouter(M):
    Z = np.zeros((np.shape(M)[0]+2,np.shape(M)[1]+2))
    for i in range(np.shape(M)[0]):
        for j in range(np.shape(M)[1]):
            Z[i+1,j+1]+=M[i,j]
    return Z




#création d'une matrice qui donne le nombre de voisin vivant de chaque cellule
def decompte_voisin_vivant(universe):
    U = ajouter(universe)
    F = np.zeros(np.shape(U))
    for i in range (1,np.shape(U)[0]-1):
        for j in range (1,np.shape(U)[1]-1):
            F[i,j]= U[i-1,j-1]+U[i-1,j]+U[i-1,j+1]+U[i,j-1]+U[i,j+1]+U[i+1,j-1]+U[i+1,j]+U[i+1,j+1]
    return F[1:np.shape(F)[0]-1,1:np.shape(F)[1]-1]




#si la cellule est vivante ou morte changement état renvoie le nouvel univers
def changement_etat(M,V):
    for i in range(np.shape(M)[0]):
        for j in range(np.shape(M)[1]):
            if M[i,j]==0 and V[i,j]==3:
                M[i,j]=1
            if M[i,j]==1 and not(V[i,j]==2 or V[i,j]==3):
                M[i,j]=0
    return M


def nouvel_univers(universe):
    M = universe
    V = decompte_voisin_vivant(universe)
    return changement_etat(M,V)

def generate(t,universe):
    U=universe
    for k in range(t):
        U=nouvel_univers(universe)
    return U







def display_the_universe():
    seed = create_seed(type_seed = "r_pentomino")
    universe = generate_universe(size=(6,6))
    universe = add_seed_to_universe(seed,universe,x_start=1,y_start=1)
    plt.imshow(universe,cmap='Greys')
    plt.show()

seeds = {
    "boat": [[1, 1, 0], [1, 0, 1], [0, 1, 0]],
    "r_pentomino": [[0, 1, 1], [1, 1, 0], [0, 1, 0]],
    "beacon": [[1, 1, 0, 0], [1, 1, 0, 0], [0, 0, 1, 1], [0, 0, 1, 1]],
    "acorn": [[0, 1, 0, 0, 0, 0, 0], [0, 0, 0, 1, 0, 0, 0], [1, 1, 0, 0, 1, 1, 1]],
    "block_switch_engine": [
        [0, 0, 0, 0, 0, 0, 1, 0],
        [0, 0, 0, 0, 1, 0, 1, 1],
        [0, 0, 0, 0, 1, 0, 1, 0],
        [0, 0, 0, 0, 1, 0, 0, 0],
        [0, 0, 1, 0, 0, 0, 0, 0],
        [1, 0, 1, 0, 0, 0, 0, 0],
    ],
    "infinite": [
        [1, 1, 1, 0, 1],
        [1, 0, 0, 0, 0],
        [0, 0, 0, 1, 1],
        [0, 1, 1, 0, 1],
        [1, 0, 1, 0, 1],
    ],
}




t=0


def game_of_life(n,p,seed,x,y,nb_generation=30,delay=300):
    universe=init_universe(seed,n,p,x,y)
    fig = plt.figure()
    im = plt.imshow(generate(t,universe),cmap='Greens', animated=True)

    def updatefig(*args):
        global t
        if t>nb_generation*2:
            exit("Fin")
        t +=1
        im.set_array(generate(t,universe))
        return im,

    ani = animation.FuncAnimation(fig, updatefig, interval=delay, blit=True)
    plt.show()

#game_of_life(10,10,"beacon",2,2,6)

def display_and_update_graphical_gameoflife(universe,n):
    gameoflife = Tk()
    f1 = Frame(gameoflife,bd=1,relief='solid')
    Label(f1,text="gameoflife").grid(row=0,column=0)
    f1.grid(row=0,column=0)
    u = Canvas(gameoflife,width=10*111,height=1010)
    u.grid(row=1,column=0)
    for k in range(n):
        a=k//10
        b=k%10
        for i in range(np.shape(universe)[0]):
            for j in range (np.shape(universe)[1]):
                if universe[i,j]==1:
                    u.create_rectangle(i*10+110*b,j*10+110*a,(i+1)*10+110*b,(j+1)*10+110*a,fill="yellow")
                else:
                    u.create_rectangle(i*10+110*b,j*10+110*a,(i+1)*10+110*b,(j+1)*10+110*a,fill="green")
        universe=nouvel_univers(universe)
    gameoflife.mainloop()

def tkinter_fenetre_evolution():
    universe=init_universe(seed,10,10,1,1)
    gameoflife = Tk()
    f1 = Frame(gameoflife,bd=1,relief='solid')
    Label(f1,text="gameoflife").grid(row=0,column=0)
    f1.grid(row=0,column=0)
    u = Canvas(gameoflife,width=101,height=101)
    u.grid(row=1,column=0)
    for k in range(n):
        time.sleep(timer/1000)
        universe=nouvel_univers(universe)
        u.delete("all")
        for i in range(np.shape(universe)[0]):
            for j in range (np.shape(universe)[1]):
                if universe[i,j]==1:
                    u.create_rectangle(i*10,j*10,(i+1)*10,(j+1)*10,fill=couleur)
                else:
                    u.create_rectangle(i*10,j*10,(i+1)*10,(j+1)*10,fill="white")
        u.update()
    gameoflife.mainloop()
#universe=init_universe("r_pentomino",10,10,1,1)
#print(tkinter_fenetre_evolution(universe,n))



def boat():
    global seed
    seed="boat"

def r_pentomino():
    global seed
    seed="r_pentomino"

def beacon():
    global seed
    seed="beacon"

def display_and_updatered():
    global couleur
    couleur="red"

def display_and_updategreen():
    global couleur
    couleur="green"

def update_label2(label, stringvar):
    global n
    n = int(stringvar.get())
    label.config(text=n)
    stringvar.set('merci')
    print(n)


root = Tk()
frame = Frame(root)
frame.pack()

rouge = tk.Button(frame,
                        text="RED",
                        fg="red",
                        command=display_and_updatered)
rouge.pack(side=tk.LEFT)

vert = tk.Button(frame,
                 text="GREEN",
                 fg="green",
                 command=display_and_updategreen)
vert.pack(side=tk.LEFT)


button = Button(frame,
                   text="QUIT",
                   activebackground = "blue",
                   fg="red",
                   command=quit)
button.pack(side=LEFT)


boat = Button(frame,
                   fg="blue",
                   text="boat",
                   command=boat)
boat.pack(side=LEFT)

r_pentomino = Button(frame,
                   fg="blue",
                   text="r_pentomino",
                   command=r_pentomino)
r_pentomino.pack(side=LEFT)

beacon = Button(frame,
                   fg="blue",
                   text="beacon",
                   command=beacon)
beacon.pack(side=LEFT)

start = Button(frame,
                   fg="blue",
                   text="start",
                   command=tkinter_fenetre_evolution)
start.pack(side=LEFT)

text2 = StringVar(root)
label2 = Label(root, text='itérations')
entry_name2 = Entry(root, textvariable=text2)
button3 = Button(root, text='clic',
                command=partial(update_label2, label2, text2))
label2.pack(side=tk.LEFT)
entry_name2.pack(side=tk.LEFT)
button3.pack(side=tk.LEFT)

def update_label2(label, stringvar):
    global n
    n = int(stringvar.get())
    label.config(text=n)
    stringvar.set('merci')
    print(n)

def update_label(label, stringvar):
    """
    Met à jour le texte d'un label en utilisant une StringVar.
    """
    global timer
    timer = int(stringvar.get())
    label.config(text=timer)
    stringvar.set('merci')
    print(timer)


text = StringVar(root)
label = Label(root, text='Delay(ms)')
entry_name = Entry(root, textvariable=text)
button2 = Button(root, text='clic',
                command=partial(update_label, label, text))
label.pack(side=LEFT)
entry_name.pack(side=LEFT)
button2.pack(side=LEFT)


root.mainloop()

